﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator_Reastaurant
{
    public  abstract class Pizza
    {
        public abstract double calculatecost();
        public abstract string getname();
        


    }

    public class Margharita : Pizza
    {
        public override double calculatecost()
        {
            return 15.00;
        }

        public override string getname()
        {
            return "Margharita(sos pomidorowy , mozzarella)";
        }

       

    }

    public class QuattroFormaggi: Pizza
    {
        public override double calculatecost()
        {
            return 17.00;
        }

        public override string getname()
        {
            return "Quattro Formaggi()";
        }

    }

    public class CAPRICCIOSA : Pizza
    {
        public override double calculatecost()
        {
            return 20.00;
        }

        public override string getname()
        {
            return "CAPRICCIOSA(sos pomidorowy, mozzarella, szynka, pieczarki)";
        }

    }

    public class Sycylijska : Pizza
    {
        public override double calculatecost()
        {
            return 24.00;
        }

        public override string getname()
        {
            return "Sycylijska(ser mozzarella, pomidorki malinowe, oliwki, anchois, cebula, oliwa z oliwek)";
        }

    }





}
